from flask import Flask, jsonify, request
from neo4j import GraphDatabase
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__)

uri = os.getenv("URI")
user = os.getenv("USER")
password = os.getenv("PASSWORD")
driver = GraphDatabase.driver(uri, auth=(user, password), database="neo4j")


def get_employees(tx, firstname, lastname, position, sortField, sortOrder):
    query = "MATCH (e:Employee) " \
            "WHERE (($firstname is null) OR (e.firstname=$firstname)) AND " \
            "(($lastname is null) OR (e.lastname=$lastname)) AND " \
            "(($position is null) OR (e.position=$position)) " \
            "RETURN e " \
            "ORDER BY e[$sortField] {}".format(sortOrder)
    results = tx.run(query, firstname=firstname, lastname=lastname, position=position, sortField=sortField, sortOrder=sortOrder).data()
    employees = [{"firstname": result['e']['firstname'], 'lastname': result['e']['lastname'], 'position': result['e']['position']} for result in results]
    return employees


@app.route('/employees', methods=['GET'])
def get_employees_route():
    firstname = request.args.get('firstname')
    lastname = request.args.get('lastname')
    position = request.args.get('position')
    sortField = request.args.get('sortField')
    sortOrder = request.args.get('sortOrder') or "ASC"

    with driver.session() as session:
        employees = session.read_transaction(get_employees, firstname, lastname, position, sortField, sortOrder)
    response = {'employees': employees}
    return jsonify(response)


def add_employee(tx, firstname, lastname, position, department):
    query = "MATCH (e:Employee {firstname: $firstname, lastname: $lastname}) RETURN e"
    result = tx.run(query, firstname=firstname, lastname=lastname).data()

    if result:
        return None
    else:
        query = "CREATE (e:Employee{firstname: $firstname, lastname: $lastname, position: $position}) " \
                "WITH e " \
                "MATCH (d:Department{name: $department})" \
                "CREATE (e)-[:WORKS_IN]->(d)"
        tx.run(query, firstname=firstname, lastname=lastname, position=position, department=department)
        return True


@app.route("/employees", methods=["POST"])
def add_employee_route():
    firstname = request.json["firstname"]
    lastname = request.json["lastname"]
    position = request.json["position"]
    department = request.json["department"]
    if firstname and lastname and position and department:
        with driver.session() as session:
            transaction = session.write_transaction(add_employee, firstname, lastname, position, department)

        if not transaction:
            response = {'message': 'Employee already in database'}
            return jsonify(response), 404
        else:
            response = {'message': 'Employee added to the database'}
            return jsonify(response)
    else:
        response = {"message": "There are some missing values"}
        return jsonify(response), 404


def update_employee(tx, id, firstname, lastname, position, department):
    query = "MATCH (e:Employee) WHERE id(e)=$id RETURN e"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (e:Employee)-[r:WORKS_IN]->(), (d:Department) " \
                "WHERE id(e)=$id and d.name=$department " \
                "CALL apoc.refactor.to(r, d) " \
                "YIELD output " \
                "SET e.firstname=$firstname, e.lastname=$lastname, e.position=$position " \
                "RETURN e, d"
        tx.run(query, id=id, firstname=firstname, lastname=lastname, position=position, department=department)
        return True


@app.route("/employees/<string:id>", methods=['PUT'])
def update_employee_route(id):
    firstname = request.json["firstname"]
    lastname = request.json["lastname"]
    position = request.json["position"]
    department = request.json["department"]

    with driver.session() as session:
        employee = session.write_transaction(update_employee, int(id), firstname, lastname, position, department)

    if not employee:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'message': 'success'}
        return jsonify(response)


def delete_employee(tx, id):
    query = "MATCH (e:Employee) WHERE id(e)=$id RETURN e"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (e:Employee) WHERE id(e)=$id DETACH DELETE e"
        tx.run(query, id=id)
        return True


@app.route("/movies/<string:id>", methods=['DELETE'])
def delete_employee_route(id):
    with driver.session() as session:
        movie = session.write_transaction(delete_employee, int(id))

    if not movie:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = {'message': 'success'}
        return jsonify(response)


def get_subordinates(tx, id):
    query = "MATCH (e:Employee) WHERE id(e)=$id RETURN e"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (e:Employee)-[:MANAGES]->(x) WHERE id(e)=$id RETURN x"
        results = tx.run(query, id=id).data()
        employees = [{"firstname": result['x']['firstname'], 'lastname': result['x']['lastname'],
                      'position': result['x']['position']} for result in results]
        return {'subordinates': employees}


@app.route('/employees/<string:id>/subordinates', methods=['GET'])
def get_subordinates_route(id):
    with driver.session() as session:
        employees = session.read_transaction(get_subordinates, int(id))

    if not employees:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = employees
        return jsonify(response)


def get_department_info(tx, id):
    query = "MATCH (e:Employee) WHERE id(e)=$id RETURN e"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (e:Employee)-[:WORKS_IN]->(d:Department) " \
                "WHERE id(e)=$id " \
                "WITH d " \
                "MATCH (e:Employee)-[:WORKS_IN]->(d) " \
                "WITH count(e) as e_count, d " \
                "MATCH (m:Employee{position:'Manager'})-[:WORKS_IN]->(d) " \
                "RETURN e_count, m.firstname, m.lastname, d.name"
        results = tx.run(query, id=id).data()
        result = [{"name": results[0]['d.name'], "manager": {"firstname": results[0]['m.firstname'], "lastname": results[0]['m.lastname']}, "number of employees": results[0]['e_count']}]
        return result


@app.route('/employees/<string:id>/department_info', methods=['GET'])
def get_department_info_route(id):
    with driver.session() as session:
        employees = session.read_transaction(get_department_info, int(id))

    if not employees:
        response = {'message': 'Employee not found'}
        return jsonify(response), 404
    else:
        response = employees
        return jsonify(response)


def get_departments(tx, name, e_count, sortField, sortOrder):
    query = "MATCH (e:Employee)-[:WORKS_IN]->(d:Department) " \
            "WITH count(e) as cnt, d " \
            "WHERE (($name is null) OR (d.name=$name)) AND " \
            "(($e_count is null) OR (cnt=$e_count)) " \
            "RETURN d, cnt " \
            # "ORDER BY e[$sortField] {}".format(sortOrder)
    if sortField == "e_count":
        query += "ORDER BY cnt {}".format(sortOrder)
    else:
        query += "ORDER BY d[$sortField] {}".format(sortOrder)

    results = tx.run(query, name=name, e_count=e_count, sortField=sortField, sortOrder=sortOrder).data()
    departments = [{"name": result['d']['name'], 'e_count': result['cnt']} for result in results]
    return departments


@app.route('/departments', methods=['GET'])
def get_departments_route():
    name = request.args.get('name')
    e_count = request.args.get('e_count')
    sortField = request.args.get('sortField')
    sortOrder = request.args.get('sortOrder') or "ASC"

    if e_count:
        e_count = int(e_count)

    with driver.session() as session:
        departments = session.read_transaction(get_departments, name, e_count, sortField, sortOrder)
    response = {'departments': departments}
    return jsonify(response)


def get_department_employees(tx, id):
    query = "MATCH (d:Department) WHERE id(d)=$id RETURN d"
    result = tx.run(query, id=id).data()

    if not result:
        return None
    else:
        query = "MATCH (d:Department)<-[:WORKS_IN]-(e) WHERE id(d)=$id RETURN e"
        results = tx.run(query, id=id).data()
        employees = [{"firstname": result['e']['firstname'], 'lastname': result['e']['lastname'],
                      'position': result['e']['position']} for result in results]
        return {'employees': employees}


@app.route('/departments/<string:id>/employees', methods=['GET'])
def get_department_employees_route(id):
    with driver.session() as session:
        employees = session.read_transaction(get_department_employees, int(id))

    if not employees:
        response = {'message': 'Department not found'}
        return jsonify(response), 404
    else:
        response = employees
        return jsonify(response)


if __name__ == '__main__':
    app.run()
